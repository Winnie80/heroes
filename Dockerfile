FROM adoptopenjdk:11-jre-openj9
COPY /target/heroes-0.0.1-SNAPSHOT.jar app-heroes.jar
ENTRYPOINT ["java", "-jar" ,"/app-heroes.jar"]