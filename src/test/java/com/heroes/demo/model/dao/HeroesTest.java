package com.heroes.demo.model.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import com.heroes.demo.model.entity.Heroe;
import com.heroes.demo.model.service.IHeroeService;

@SpringBootTest(properties = {"spring.jpa.database-platform=org.hibernate.dialect.H2Dialect"})
@Sql("/import.sql")	
@AutoConfigureTestDatabase
public class HeroesTest {
	
	@Autowired
	IHeroeService service;
	
	@Test
    void testHeroes() {
		
		Heroe heroe =  service.findById(Long.valueOf(1));
		assertTrue(heroe.getPoder().equals("VOLAR")); 
		
		
		Heroe heroeNew = new Heroe();
		heroeNew.setNombre("Batman");
		heroeNew.setPoder("PELEAR");
		service.save(heroeNew);
		
		 Optional<Heroe> batman = service.findAll()
         .stream()
         .filter(e -> e.getNombre().contains("Bat"))
         .findFirst();

        assertTrue(batman.isPresent());
        assertEquals("PELEAR", batman.get().getPoder());
        assertEquals(1, batman.orElseThrow().getId(), () -> "No soy Superman.");
        
    }

}
