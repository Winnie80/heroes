package com.heroes.demo.search;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


public class SearchSpecifications<T> implements Specification<T> {

    private static final long serialVersionUID = 1L;
    
	private List<SearchCriteria> searchCriteriaList;

    public SearchSpecifications() {
        this.searchCriteriaList = new ArrayList<>();
    }

    public void add(SearchCriteria criteria) {
        searchCriteriaList.add(criteria);
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();
        for (SearchCriteria criteria : searchCriteriaList)
            
           if (criteria.getSearchFilter().equals(SearchFilter.CONTENT)) {
                    predicates.add(builder.like(builder.lower(root.get(criteria.getAttr())),"%" + criteria.getValue().toString().toLowerCase() + "%"));
    		}
                
                        
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
