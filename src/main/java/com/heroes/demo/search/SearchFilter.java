package com.heroes.demo.search;

public enum  SearchFilter {

	 EQUAL,
	 CONTENT,
	 MATCH_END,
	 IN,
}
