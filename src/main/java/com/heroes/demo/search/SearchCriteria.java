package com.heroes.demo.search;

public class SearchCriteria {

	private String attr;
	private Object value;
	private SearchFilter searchFilter;

	public SearchCriteria() {
	}

	public SearchCriteria(String attr, Object value, SearchFilter searchFilter) {
		this.attr = attr;
		this.value = value;
		this.searchFilter = searchFilter;
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public SearchFilter getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(SearchFilter searchFilter) {
		this.searchFilter = searchFilter;
	}

}
