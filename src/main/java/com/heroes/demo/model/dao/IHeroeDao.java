package com.heroes.demo.model.dao;

import java.util.Optional;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.heroes.demo.model.entity.Heroe;

@Repository
public interface IHeroeDao extends CrudRepository<Heroe, Long>, JpaSpecificationExecutor<Heroe> {
	
	
}
