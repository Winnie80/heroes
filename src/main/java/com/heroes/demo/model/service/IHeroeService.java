package com.heroes.demo.model.service;

import java.util.List;

import com.heroes.demo.model.entity.Heroe;

public interface IHeroeService  {
	
	public List<Heroe> findAll();
	
	public List<Heroe> findHeroesWithPartOfName(String nombre);
	
	public Heroe findById(Long id);
	
	public Heroe save(Heroe heroe);
	
	public void delete(Long id);

}
