package com.heroes.demo.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.heroes.demo.model.dao.IHeroeDao;
import com.heroes.demo.model.entity.Heroe;
import com.heroes.demo.search.SearchCriteria;
import com.heroes.demo.search.SearchFilter;
import com.heroes.demo.search.SearchSpecifications;

@Service
public class HeroeServiceImpl implements IHeroeService {

	@Autowired
	private IHeroeDao heroeDao;

	@Override
	@Transactional(readOnly = true)
	@Cacheable(value="heroes")    
	public List<Heroe> findAll() {
		return (List<Heroe>) heroeDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	@Cacheable(value="heroes")   
	public Heroe findById(Long id) {
		return heroeDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	@CacheEvict(value = "heroes",allEntries = true) 
	public Heroe save(Heroe cliente) {
		return heroeDao.save(cliente);
	}

	@Override
	@Transactional
	@CacheEvict(value = "heroes",allEntries = true) 
	public void delete(Long id) {
		heroeDao.deleteById(id);
	}

	@Override
	public List<Heroe> findHeroesWithPartOfName(String nombre) {
		SearchSpecifications<Heroe> searchSpecifications = new SearchSpecifications<>();
		searchSpecifications.add(new SearchCriteria("nombre", nombre, SearchFilter.CONTENT));
		return heroeDao.findAll(searchSpecifications);
	}

}
