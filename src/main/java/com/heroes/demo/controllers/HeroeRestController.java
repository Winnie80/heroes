package com.heroes.demo.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.heroes.demo.model.entity.Heroe;
import com.heroes.demo.model.service.IHeroeService;




@RestController
@RequestMapping("/api")
public class HeroeRestController {
	
	@Autowired
	private IHeroeService heroeService;

	@GetMapping("/heroes")
	public List<Heroe> findAll(@RequestParam(name="name",required = false) String name){
		return name !=null ? heroeService.findHeroesWithPartOfName(name):heroeService.findAll();
		
	}
	
	@GetMapping("/heroes/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		
		Heroe heroe = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			heroe = heroeService.findById(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(heroe == null) {
			response.put("mensaje", "El super héroe ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Heroe>(heroe, HttpStatus.OK);
	}
	
	@PostMapping("/heroes")
	public ResponseEntity<?> create(@Valid @RequestBody Heroe heroe, BindingResult result) {
		
		Heroe heroeNew = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			heroeNew = heroeService.save(heroe);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El heróe ha sido creado con éxito!");
		response.put("heroe", heroeNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/heroes/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Heroe heroe, BindingResult result, @PathVariable Long id) {

		Heroe heroeActual = heroeService.findById(id);

		Heroe heroeUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (heroeActual == null) {
			response.put("mensaje", "Error: no se pudo editar, el heróe ID: "
					.concat(id.toString().concat(" no está en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			heroeActual.setNombre(heroe.getNombre());
			heroeActual.setPoder(heroe.getPoder());
			heroeUpdated = heroeService.save(heroeActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el heróe en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El heróe ha sido actualizado con éxito!");
		response.put("heroe", heroeUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/heroes/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		Map<String, Object> response = new HashMap<>();
		
		try {
		    heroeService.delete(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar el heróe de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El héroe eliminado con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
